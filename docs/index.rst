.. PLUC documentation master file, created by
   sphinx-quickstart on Tue Aug 11 08:54:26 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PLUC's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
This is the documentation for the PLUC. Use this to help you get started building and using your PLUC, and troubleshooting any problems you might have.
