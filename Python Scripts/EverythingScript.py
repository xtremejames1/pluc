#!/usr/bin/python
from RPLCD.i2c import CharLCD
import RPi.GPIO as GPIO
from time import *

GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering

#Button Setup:
upbutton = 7
rightbutton = 8
leftbutton = 10
downbutton = 12
selectbutton = 11
GPIO.setup(upbutton, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(rightbutton, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(leftbutton, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(downbutton, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(selectbutton, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

#LCD Custom characters

capslocked=(0x4,0xe,0x1f,0xe,0xe,0x0,0xe) #Capslocked\
nocaps=(0x0,0x4,0xa,0x1b,0xa,0xe,0x0) #Nocaps
enter=(0x0,0x0,0x1,0x9,0x1f,0x8,0x0) #enter
letters=(0x8,0x14,0x1c,0x14,0x6,0x5,0x6) #numbers
backspace=(0x0,0x0,0x8,0x1f,0x8,0x0,0x0) #backspace
symbols=(0x11,0x9,0x8,0x4,0x2,0x1a,0x19) #backspace
lcda.create_char(capslocked)
lcda.create_char(nocaps)
lcda.create_char(enter)
lcda.create_char(letters)
lcda.create_char(backspace)
lcda.create_char(symbols)

#LCD Initialization
lcda = CharLCD('PCF8574', 0x3f)
lcda.lcd_display_string_pos("Welcome to PLUC!", 0, 1)
sleep(1.5)
lcda.clear()

#Words
def presetsword(line, char, select):
    if select == 0:
        lcda.lcd_display_string_pos("PRESETS", line, char)
    elif select == 1:
        lcda.lcd_display_string_pos("PRESETS    <", line, char)
def networkword(line, char, select):
    if select == 0:
        lcda.lcd_display_string_pos("NETWORK", line, char)
    elif select == 1:
        lcda.lcd_display_string_pos("NETWORK    <", line, char)

#More Variables
networkmenuin == False
presetsmenuin == False
initmenuin == False

#Keyboard
input = ""   #What is typed out
keyboardrow = 0
keyboardcolumn = 0
keyboardenabled = True
keyboardchange = False
keyboardmenu = 0
capson = False
row1letters = [False, False, False, False, False, False, False, False, False, False]   #which thing is presed
row2letters = [False, False, False, False, False, False, False, False, False, False]
row3letters = [False, False, False, False, False, False, False, False, False]
row1numbers = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]   #which thing is presed
row2numbers = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]
row3numbers = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]
keyq = row1letters(0)      #makes variables for each 'key'
keyw = row1letters(1)
keye = row1letters(2)
keyr = row1letters(3)
keyt = row1letters(4)
keyy = row1letters(5)
keyu = row1letters(6)
keyi = row1letters(7)
keyo = row1letters(8)
keyp = row1letters(9)
keya = row2letters(1)
keys = row2letters(2)
keyd = row2letters(3)
keyf = row2letters(4)
keyg = row2letters(5)
keyh = row2letters(6)
keyj = row2letters(7)
keyk = row2letters(8)
keyl = row2letters(9)
keyz = row3letters(1)
keyx = row3letters(2)
keyc = row3letters(3)
keyv = row3letters(4)
keyb = row3letters(5)
keyn = row3letters(6)
keym = row3letters(7)
keySymbol = row3letters(0)
keyEnter = row2letters(10)
keyCaps = row2letters(0)
keyBackspace = row1letters(10)

key1 = row1numbers(0)
key2 = row1numbers(1)
key3 = row1numbers(2)
key4 = row1numbers(3)
key5 = row1numbers(4)
key6 = row1numbers(5)
key7 = row1numbers(6)
key8 = row1numbers(7)
key9 = row1numbers(8)
key0 = row1numbers(9)
key- = row1numbers(10)
key+ = row1numbers(11)
key/ = row1numbers(12)
key? = row1numbers(13)
key` = row2numbers(0)
key~ = row2numbers(1)
key! = row2numbers(2)
key@ = row2numbers(3)
keyhash = row2numbers(4)
key$ = row2numbers(5)
key% = row2numbers(6)
key^ = row2numbers(7)
key& = row2numbers(8)
key* = row2numbers(9)
key( = row2numbers(10)
key) = row2numbers(11)
key< = row2numbers(12)
key> = row2numbers(13)
key= = row3numbers(1)
key_ = row3numbers(2)
key[ = row3numbers(3)
key] = row3numbers(4)
key{ = row3numbers(5)
key} = row3numbers(6)
key\ = row3numbers(7)
key| = row3numbers(8)
key; = row3numbers(9)
key: = row3numbers(10)
key' = row3numbers(11)
key" = row3numbers(12)
key, = row3numbers(13)
key. = row3numbers(14)
keyBackspaceNum = row1numbers(14)
keyEnterNum = row2numbers(14)
keyLetters = row3numbers(0)
if keyboardenabled:
    if GPIO.input(downbutton) == GPIO.HIGH or GPIO.input(upbutton) == GPIO.HIGH or GPIO.input(rightbutton) == GPIO.HIGH or GPIO.input(leftbutton) == GPIO.HIGH or GPIO.input(selectbutton) == GPIO.HIGH:
        if GPIO.input(downbutton) == GPIO.HIGH:
            keyboardcolumn-=1
        if GPIO.input(upbutton) == GPIO.HIGH:
            keyboardcolumn+=1
        if GPIO.input(rightbutton) == GPIO.HIGH:
            keyboardrow+=1
        if GPIO.input(leftbutton) == GPIO.HIGH:
            keyboardrow-=1
        row1letters = [False, False, False, False, False, False, False, False, False, False]   #Resets everything
        row2letters = [False, False, False, False, False, False, False, False, False, False]
        row3letters = [False, False, False, False, False, False, False, False, False]
        if keyboardrow == 0:                        #This figures out which row/column the selection is on, and changes it to true
            row1(keyboardcolumn) = True
        elif keyboardrow == 1:
            row2(keyboardcolumn) = True
        elif keyboardrow == 2:
            row3(keyboardcolumn) = True
    if keyboardmenu == 0:
        if capson:
            if keyq:
                lcd_display_string_pos("-", 1, 0)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "Q"
            else:
                lcd_display_string_pos("Q", 1, 0)
            if keyw:
                lcd_display_string_pos("-", 1, 1)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "W"
            else:
                lcd_display_string_pos("W", 1, 1)
            if keye:
                lcd_display_string_pos("-", 1, 2)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "E"
            else:
                lcd_display_string_pos("E", 1, 2)
            if keyr:
                lcd_display_string_pos("-", 1, 3)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "R"
            else:
                lcd_display_string_pos("R", 1, 3)
            if keyt:
                lcd_display_string_pos("-", 1, 4)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "T"
            else:
                lcd_display_string_pos("T", 1, 4)
            if keyy:
                lcd_display_string_pos("-", 1, 5)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "Y"
            else:
                lcd_display_string_pos("Y", 1, 5)
            if keyu:
                lcd_display_string_pos("-", 1, 6)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "U"
            else:
                lcd_display_string_pos("U", 1, 6)
            if keyi:
                lcd_display_string_pos("-", 1, 7)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "I"
            else:
                lcd_display_string_pos("I", 1, 7)
            if keyo:
                lcd_display_string_pos("-", 1, 8)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "O"
            else:
                lcd_display_string_pos("O", 1, 8)
            if keyp:
                lcd_display_string_pos("-", 1, 9)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "P"
            else:
                lcd_display_string_pos("P", 1, 9)
            if keya:
                lcd_display_string_pos("-", 2, 1)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "A"
            else:
                lcd_display_string_pos("A", 2, 1)
            if keys:
                lcd_display_string_pos("-", 2, 2)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "S"
            else:
                lcd_display_string_pos("S", 2, 2)
            if keyd:
                lcd_display_string_pos("-", 2, 3)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "D"
            else:
                lcd_display_string_pos("D", 2, 3)
            if keyf:
                lcd_display_string_pos("-", 2, 4)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "F"
            else:
                lcd_display_string_pos("F", 2, 4)
            if keyg:
                lcd_display_string_pos("-", 2, 5)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "G"
            else:
                lcd_display_string_pos("G", 2, 5)
            if keyh:
                lcd_display_string_pos("-", 2, 6)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "H"
            else:
                lcd_display_string_pos("H", 2, 6)
            if keyj:
                lcd_display_string_pos("-", 2, 7)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "J"
            else:
                lcd_display_string_pos("J", 2, 7)
            if keyk:
                lcd_display_string_pos("-", 2, 8)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "K"
            else:
                lcd_display_string_pos("K", 2, 8)
            if keyl:
                lcd_display_string_pos("-", 2, 9)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "L"
            else:
                lcd_display_string_pos("L", 2, 9)
            if keyz:
                lcd_display_string_pos("-", 3, 1)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "Z"
            else:
                lcd_display_string_pos("Z", 3, 1)
            if keyx:
                lcd_display_string_pos("-", 3, 2)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "X"
            else:
                lcd_display_string_pos("X", 3, 2)
            if keyc:
                lcd_display_string_pos("-", 3, 3)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "C"
            else:
                lcd_display_string_pos("C", 3, 3)
            if keyv:
                lcd_display_string_pos("-", 3, 4)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "V"
            else:
                lcd_display_string_pos("V", 3, 4)
            if keyb:
                lcd_display_string_pos("-", 3, 5)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "B"
            else:
                lcd_display_string_pos("B", 3, 5)
            if keyn:
                lcd_display_string_pos("-", 3, 6)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "N"
            else:
                lcd_display_string_pos("N", 3, 6)
            if keym:
                lcd_display_string_pos("-", 3, 7)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "M"
            else:
                lcd_display_string_pos("M", 3, 7)
            if keyBackspace:
                lcd_display_string_pos("-", 1, 10)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    string[:-1]
            else:
                lcda.write_string("\x4", 1, 10)
            if keyCaps:
                lcd_display_string_pos("-", 1, )
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    capson = not capson
            else:
                if capson:
                    lcda.write_string_pos("\x0", 2, 0)
                else:
                    lcda.write_string_pos("\x1", 2, 0)
            if keyEnter:                                        #Enter Key
                lcd_display_string_pos("-", 2, 10)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    keyboardenabled = False                     #When pressed, disable the keyboard, then return what was typed (input)
                    return input
            else:
                lcd_display_string_pos("\x1", 2, 10)
            if keySymbol:
                lcd_display_string_pos("-", 3, 0)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    keyboardmenu = 1
            else:
                lcda.write_string_pos("\x5", 3, 0)
        else:
            if keyq:
                lcd_display_string_pos("-", 1, 0)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "q"
            else:
                lcd_display_string_pos("q", 1, 0)
            if keyw:
                lcd_display_string_pos("-", 1, 1)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "w"
            else:
                lcd_display_string_pos("w", 1, 1)
            if keye:
                lcd_display_string_pos("-", 1, 2)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "e"
            else:
                lcd_display_string_pos("e", 1, 2)
            if keyr:
                lcd_display_string_pos("-", 1, 3)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "r"
            else:
                lcd_display_string_pos("r", 1, 3)
            if keyt:
                lcd_display_string_pos("-", 1, 4)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "t"
            else:
                lcd_display_string_pos("t", 1, 4)
            if keyy:
                lcd_display_string_pos("-", 1, 5)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "y"
            else:
                lcd_display_string_pos("y", 1, 5)
            if keyu:
                lcd_display_string_pos("-", 1, 6)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "u"
            else:
                lcd_display_string_pos("u", 1, 6)
            if keyi:
                lcd_display_string_pos("-", 1, 7)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "i"
            else:
                lcd_display_string_pos("i", 1, 7)
            if keyo:
                lcd_display_string_pos("-", 1, 8)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "o"
            else:
                lcd_display_string_pos("o", 1, 8)
            if keyp:
                lcd_display_string_pos("-", 1, 9)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "p"
            else:
                lcd_display_string_pos("p", 1, 9)
            if keya:
                lcd_display_string_pos("-", 2, 1)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "a"
            else:
                lcd_display_string_pos("a", 2, 1)
            if keys:
                lcd_display_string_pos("-", 2, 2)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "s"
            else:
                lcd_display_string_pos("s", 2, 2)
            if keyd:
                lcd_display_string_pos("-", 2, 3)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "d"
            else:
                lcd_display_string_pos("d", 2, 3)
            if keyf:
                lcd_display_string_pos("-", 2, 4)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "f"
            else:
                lcd_display_string_pos("f", 2, 4)
            if keyg:
                lcd_display_string_pos("-", 2, 5)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "g"
            else:
                lcd_display_string_pos("g", 2, 5)
            if keyh:
                lcd_display_string_pos("-", 2, 6)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "h"
            else:
                lcd_display_string_pos("h", 2, 6)
            if keyj:
                lcd_display_string_pos("-", 2, 7)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "j"
            else:
                lcd_display_string_pos("j", 2, 7)
            if keyk:
                lcd_display_string_pos("-", 2, 8)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "k"
            else:
                lcd_display_string_pos("k", 2, 8)
            if keyl:
                lcd_display_string_pos("-", 2, 9)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "l"
            else:
                lcd_display_string_pos("l", 2, 9)
            if keyz:
                lcd_display_string_pos("-", 3, 1)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "z"
            else:
                lcd_display_string_pos("z", 3, 1)
            if keyx:
                lcd_display_string_pos("-", 3, 2)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "x"
            else:
                lcd_display_string_pos("x", 3, 2)
            if keyc:
                lcd_display_string_pos("-", 3, 3)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "c"
            else:
                lcd_display_string_pos("c", 3, 3)
            if keyv:
                lcd_display_string_pos("-", 3, 4)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "v"
            else:
                lcd_display_string_pos("v", 3, 4)
            if keyb:
                lcd_display_string_pos("-", 3, 5)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "b"
            else:
                lcd_display_string_pos("b", 3, 5)
            if keyn:
                lcd_display_string_pos("-", 3, 6)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "n"
            else:
                lcd_display_string_pos("n", 3, 6)
            if keym:
                lcd_display_string_pos("-", 3, 7)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    input += "m"
            else:
                lcd_display_string_pos("m", 3, 7)
            if keyBackspace:
                lcd_display_string_pos("-", 1, 10)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    string[:-1]
            else:
                lcda.write_string("\x4", 1, 10)
            if keyCaps:
                lcd_display_string_pos("-", 1, )
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    capson = not capson
            else:
                if capson:
                    lcda.write_string_pos("\x0", 2, 0)
                else:
                    lcda.write_string_pos("\x1", 2, 0)
            if keyEnter:                                        #Enter Key
                lcd_display_string_pos("-", 2, 10)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    keyboardenabled = False                    #When pressed, disable the keyboard, then return what was typed (input)
                    return input
            else:
                lcd_display_string_pos("\x1", 2, 10)
            if keySymbol:
                lcd_display_string_pos("-", 3, 0)
                if GPIO.input(selectbutton) == GPIO.HIGH:
                    keyboardmenu = 1
            else:
                lcda.write_string_pos("\x5", 3, 0)
    elif keyboardmenu == 1:
        if key1:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "1"
        else:
            lcd_display_string_pos("1", 1, 0)
        if key2:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "2"
        else:
            lcd_display_string_pos("2", 1, 0)
        if key3:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "3"
        else:
            lcd_display_string_pos("3", 1, 0)
        if key4:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "4"
        else:
            lcd_display_string_pos("4", 1, 0)
        if key5:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "5"
        else:
            lcd_display_string_pos("5", 1, 0)
        if key6:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "6"
        else:
            lcd_display_string_pos("6", 1, 0)
        if key7:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "7"
        else:
            lcd_display_string_pos("7", 1, 0)
        if key8:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "8"
        else:
            lcd_display_string_pos("8", 1, 0)
        if key9:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "9"
        else:
            lcd_display_string_pos("9", 1, 0)
        if key0:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "0"
        else:
            lcd_display_string_pos("0", 1, 0)
        if key+:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "+"
        else:
            lcd_display_string_pos("+", 1, 0)
        if key-:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "-"
        else:
            lcd_display_string_pos("-", 1, 0)
        if key/:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "/"
        else:
            lcd_display_string_pos("/", 1, 0)
        if key?:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "?"
        else:
            lcd_display_string_pos("?", 1, 0)
        if key`:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "`"
        else:
            lcd_display_string_pos("`", 1, 0)
        if key~:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "~"
        else:
            lcd_display_string_pos("~", 1, 0)
        if key!:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "!"
        else:
            lcd_display_string_pos("!", 1, 0)
        if key@:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "@"
        else:
            lcd_display_string_pos("@", 1, 0)
        if keyhash:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "#"
        else:
            lcd_display_string_pos("#", 1, 0)
        if key$:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "$"
        else:
            lcd_display_string_pos("$", 1, 0)
        if key%:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "%"
        else:
            lcd_display_string_pos("%", 1, 0)
        if key^:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "^"
        else:
            lcd_display_string_pos("^", 1, 0)
        if key&:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "&"
        else:
            lcd_display_string_pos("&", 1, 0)
        if key*:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "*"
        else:
            lcd_display_string_pos("*", 1, 0)
        if key(:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "("
        else:
            lcd_display_string_pos("(", 1, 0)
        if key):
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += ")"
        else:
            lcd_display_string_pos(")", 1, 0)
        if key<:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "<"
        else:
            lcd_display_string_pos("<", 1, 0)
        if key>:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += ">"
        else:
            lcd_display_string_pos(">", 1, 0)
        if key=:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "="
        else:
            lcd_display_string_pos("=", 1, 0)
        if key_:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "_"
        else:
            lcd_display_string_pos("_", 1, 0)
        if key[:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "["
        else:
            lcd_display_string_pos("[", 1, 0)
        if key]:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "]"
        else:
            lcd_display_string_pos("]", 1, 0)
        if key{:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "{"
        else:
            lcd_display_string_pos("{", 1, 0)
        if key}:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "}"
        else:
            lcd_display_string_pos("}", 1, 0)
        if key\:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "\\"
        else:
            lcd_display_string_pos("\\", 1, 0)
        if key|:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "|"
        else:
            lcd_display_string_pos("|", 1, 0)
        if key;:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += ";"
        else:
            lcd_display_string_pos(";", 1, 0)
        if key::
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += ":"
        else:
            lcd_display_string_pos(":", 1, 0)
        if key':
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "'"
        else:
            lcd_display_string_pos("'", 1, 0)
        if key":
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "\""
        else:
            lcd_display_string_pos("\"", 1, 0)
        if key,:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += ","
        else:
            lcd_display_string_pos(",", 1, 0)
        if key.:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                input += "."
        else:
            lcd_display_string_pos(".", 1, 0)
        if keyBackspaceNum:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                string[:-1]
        else:
            lcd_display_string_pos("\x4", 1, 0)
        if keyEnterNum:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                keyboardenabled = False
                return input
        else:
            lcd_display_string_pos("\x2", 1, 0)
        if keyLetters:
            lcd_display_string_pos("-", 1, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                keyboardmenu = 0
        else:
            lcd_display_string_pos("\x3", 1, 0)
#First Menu
def initmenu():
    initmenuin = True#whether the user is in this menu
    initmenuselect = 0#defines which option is selected
    while initmenuin:
        if initmenuselect == 0: #Which menu is selected
            lcda.clear()
            lcda.lcd_display_string_pos("Start Menu", 0)
            presetsword(1, 0, 1) #Highlight PRESETS
            networkword(2, 0, 0)
            if GPIO.input(selectbutton) == GPIO.HIGH:
                initmenuin == False
                presetsmenuin == False
                return
        elif initmenuselect == 1:
            lcda.clear()
            presetsword(1, 0, 0)
            networkword(2, 0, 1) #Highlight NETWORK
            if GPIO.input(selectbutton) == GPIO.HIGH:
                initmenuin == False
                networkmenuin == False
                return
        if initmenuselect < 0: #Makes min value
            initmenuselect = 0
        if initmenuselect > 1: #Makes max value
            initmenuselect = 1
        if GPIO.input(downbutton) == GPIO.HIGH: #Decreases selection value with button
            initmenuselect -=1
            sleep(.3)
        elif GPIO.input(upbutton) == GPIO.HIGH: #Increases selection value with button
            initmenuselect +=1
            sleep(.3)
